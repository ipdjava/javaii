package exam;

public class Address {
	
	private String streetAddress;
	private int bulingNumber;
	
	public String getStreetAddress() {
		return streetAddress;
	}
	
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	
	public int getBulingNumber() {
		return bulingNumber;
	}
	
	public void setBulingNumber(int bulingNumber) {
		this.bulingNumber = bulingNumber;
	}
	
	public Address(String streetAddress, int bulingNumber) {
		super();
		this.streetAddress = streetAddress;
		this.bulingNumber = bulingNumber;
	}
	
}
