package question6PC;

public class Monitor {
	private String model;
	private String manufacturer;
	private int size;
	private Resolution resolution;
 
	public Monitor(String model, String manufacturer,int size, Resolution resolution) {
		super();
		this.model = model;
		this.manufacturer = manufacturer;
		this.size = size ;
		this.resolution = resolution;
	}
 
	
 
	public String getModel() {
		return model;
	}
 
	public String getManufacturer() {
		return manufacturer;
	}
 
	public Resolution getResolution() {
		return resolution;
	}
 
}
