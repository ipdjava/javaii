package question6PC;

public class PC {
	private Monitor newMonitor;
	private Moterboard newMotherboard;
	private Case newCase;
	
public PC (Case newCase, Monitor newMonitor, Moterboard newMotherboard) {
	this.newCase= newCase;
	this.newMonitor= newMonitor;
	this.newMotherboard= newMotherboard;
}

public Monitor getNewMonitor() {
	return newMonitor;
}

public void setNewMonitor(Monitor newMonitor) {
	this.newMonitor = newMonitor;
}

public Moterboard getNewMotherboard() {
	return newMotherboard;
}

public void setNewMotherboard(Moterboard newMotherboard) {
	this.newMotherboard = newMotherboard;
}

public Case getNewCase() {
	return newCase;
}

public void setNewCase(Case newCase) {
	this.newCase = newCase;
}
}

