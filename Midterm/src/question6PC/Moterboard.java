package question6PC;

public class Moterboard {
	
	private String model;
	private String manufacturer;
	private String bios;
	private int ramSlots;
	private int  cardSlots;
 
	public void Motherboard(String model, String manufacturer, String bios, int ramSlots, int cardSlots) {
		
		this.model = model;
		this.manufacturer = manufacturer;
		this.bios = bios;
		this.ramSlots = ramSlots;
		this.cardSlots = cardSlots;
	}
 
	
	
 
	public String getModel() {
		return model;
	}
 
	public String getManufacturer() {
		return manufacturer;
	}
 
	public String getBiosName() {
		return bios;
	}
 
	public int getRamSlots() {
		return ramSlots;
	}
 
	public int cardSlots() {
		return cardSlots;
	}
 
}

