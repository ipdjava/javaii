package question6PC;

public class Case {
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Dimension getDimension() {
		return dimension;
	}

	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}

	public String getPowerSupply() {
		return powerSupply;
	}

	public void setPowerSupply(String powerSupply) {
		this.powerSupply = powerSupply;
	}

	private String model;
	private String manufacturer;
	private Dimension dimension;
	private String powerSupply;
 
	public Case(String model, String manufacturer, Dimension dimension, String powerSupply) {
		super();
		this.model = model;
		this.manufacturer = manufacturer;
		this.dimension = dimension;
		this.powerSupply = powerSupply;
	}
 
	public void pressPowerButton() {
		System.out.println("Power Button Pressed");
	}
 
}

