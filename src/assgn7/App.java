package assgn7;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		
		List<User> userList = new ArrayList<User>();
		LocalDate birthday1 = LocalDate.of(2020, Month.JANUARY, 8);
		userList.add(new User(23,"Joe",birthday1));
		LocalDate birthday2 = LocalDate.of(2020, Month.FEBRUARY, 9);
		userList.add(new User(23,"Rick",birthday2));
		LocalDate birthday3 = LocalDate.of(2020, Month.MARCH, 10);
		userList.add(new User(23,"Ron",birthday3));
		LocalDate birthday4 = LocalDate.of(2020, Month.APRIL, 11);
		userList.add(new User(23,"Clark",birthday4));
		LocalDate birthday5 = LocalDate.of(2020, Month.MAY, 12);
		userList.add(new User(23,"Ralph",birthday5));
		
		userList.sort((User u1, User u2)->u1.getId()-u2.getId()); 
	      userList.forEach((u)->System.out.println(u)); 
		
	}

}
