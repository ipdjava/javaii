package filewriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Stream;

public class App {
	
	public static void writeToFile(ArrayList <Address> alAdd) throws IOException {
		
		File myObj = new File("src/resources/address.txt");
		
		try {
		      if (myObj.createNewFile()) {
		        System.out.println("File created: " + myObj.getName());
		      } else {
		        System.out.println("File already exists.");
		      }
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		  
		
		  try (BufferedWriter writer = new BufferedWriter(new FileWriter(myObj.getAbsolutePath()))) {
			  for(Address a : alAdd) {
				  writer.write(a.getBuildingNo() + a.getStreetName() + a.getCityName() + a.getProvinceName() + a.getPostalCode() + "\n");
			  } 
		  } catch (IOException e) {
			  System.out.println("An error occured.");
			  e.printStackTrace();
		  }
	}
	
	public static void readFromFile() throws IOException {
		try(BufferedReader reader = new  BufferedReader(new FileReader("src/resources/address.txt"))){
            Stream<String> lines = reader.lines();
            lines.forEach(line -> System.out.println(line));
        } catch (IOException e) {
            throw e;
        }
	}
	
	

	public static void main(String[] args) {
		
ArrayList<Address> addresses = new ArrayList<Address>(); 
		
addresses.add(new Address ("534", " D'Youville", "Montreal", "Quebec", "H1A 2CD"));
addresses.add(new Address ("754", " Monday Ave", "Scotsdale", "Ontario", "A3E 4F5"));
addresses.add(new Address ("9634", " Wired Steet", "King's Landing", "New Brunswick", "G6H 7I9"));
addresses.add(new Address ("766", " Brown Ave", "Halifax", "Nova Scotia", "J1K 2L3"));
addresses.add(new Address ("365", " 12th Avenue", "Malpeque", "PEI", "M4N 5O6"));
		
		try {
			writeToFile(addresses);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			readFromFile();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

}