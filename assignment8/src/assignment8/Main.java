package assignment8;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Stream;

public class Main {
	
	public static void writeToFile(ArrayList <Address> alAdd) throws IOException {
		
		File myObj = new File("src/resource/address.txt");
		
		try {
		      if (myObj.createNewFile()) {
		        System.out.println("File created: " + myObj.getName());
		      } else {
		        System.out.println("File already exists.");
		      }
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		  
		
		  try (BufferedWriter writer = new BufferedWriter(new FileWriter(myObj.getAbsolutePath()))) {
			  for(Address a : alAdd) {
				  writer.write(a.getBuildingNo() + a.getStreetName() + a.getCityName() + a.getProvinceName() + a.getPostalCode() + "\n");
			  } 
		  } catch (IOException e) {
			  System.out.println("An error occured.");
			  e.printStackTrace();
		  }
	}
	
	public static void readFromFile() throws IOException {
		try(BufferedReader reader = new  BufferedReader(new FileReader("src/resource/address.txt"))){
            Stream<String> lines = reader.lines();
            lines.forEach(line -> System.out.println(line));
        } catch (IOException e) {
            throw e;
        }
	}
	
	
	public static void main(String[] args) {
		
		

		ArrayList<Address> alAdd = new ArrayList<Address>(); 
		
		alAdd.add(new Address ("951", "Red Street", "Pooville", "Quebec", "H1A 2CD"));
		alAdd.add(new Address ("123", "Blue Street", "Muffinville", "Ontario", "A3E 4F5"));
		alAdd.add(new Address ("456", "Green Street", "Socktown", "New Brunswick", "G6H 7I9"));
		alAdd.add(new Address ("789", "Yellow Street", "Mochiville", "Nova Scotia", "J1K 2L3"));
		alAdd.add(new Address ("753", "Orange Street", "Hatland", "PEI", "M4N 5O6"));
		
		try {
			writeToFile(alAdd);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			readFromFile();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

}
